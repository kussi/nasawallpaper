Finds the NASA Picture of the Day and downloads it.

Please make a .env file and insert you NASA API key:

API_KEY=...

You can get one easily from NASA Open APIs.

You can optionally add the name, folder and the format for the image.

NAME=nasawallpaper

FORMAT=png

DIR=./
